//: Создайте по 1-2 enum разных типов
enum Vehicle {
    case car
    case bike
    case bicycle
    case plane
}

let harleyDavidson = Vehicle.bike

enum Family {
    case mom
    case dad
    case brother
    case gran
}

let mom: Family = .mom


//: Создайте несколько своих enum
enum MorningTreatments {
    case washing
    case breakfast
    case coffee
}

enum Resume {
    case name(String)
    case surname(String)
    case experience(Int)
    case stak(String)
    case salary(Double)
}

let oleg = Resume.experience(5)

//: Создать enum со всеми цветами радуги
enum Colour {
    case red, orange, yellow, green, blue, indigo, violet
}

func printItem(colourCheck: Colour) {
    
    switch colourCheck {
    case .red:
        print("Sun is \(colourCheck)")
    case .orange:
        print("orange is \(colourCheck)")
    case .yellow:
        print("banana is \(colourCheck)")
    case .green:
        print("apple is \(colourCheck)")
    case .blue:
        print("butterfly is \(colourCheck)")
    case .indigo:
        print("sky is \(colourCheck)")
    case .violet:
        print("flower is \(colourCheck)")
    }

}

printItem(colourCheck: .violet)


//: Создать функцию, которая выставляет оценки ученикам в школе.
enum Estimation {
    case five
    case four
    case three
    case two
    
    enum SchoolSubjects {
        case math
        case physics
        case programming
        case economics
    }
    
}


func addScore(name: String, score: (Estimation, Estimation.SchoolSubjects)) {
    
    switch score.0 {
    case .five:
        print("\(name) has \(score.0) in \(score.1)")
    case .four:
        print("\(name) has \(score.0) in \(score.1)")
    case .three:
        print("\(name) has \(score.0) in \(score.1)")
    case .two:
        print("\(name) has \(score.0) in \(score.1)")
    }
}

addScore(name: "Maria", score: (.five, .programming))
addScore(name: "Oleg", score: (.three, .physics))
addScore(name: "Andrew", score: (.five, .economics))

//: Создать программу, которая "рассказывает" - какие автомобили стоят в гараже.
enum Garage: String, CaseIterable {
    case mercedes = "AMG"
    case bmw = "i8"
    case mazda = "cx-5"
    
}

func aboutCarsInGarage() {
    for item in Garage.allCases {
        print("In garage there is \(item.rawValue) ")
    }
}

aboutCarsInGarage()


